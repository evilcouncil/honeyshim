package main

const (
	LOCALPORT              = "LOCAL_PORT"
	DEFAULTLOCALPORT       = ":8080"
	TARGETAPP              = "TARGET_APP"
	DEFAULTTARGETAPP       = "http://localhost:8000"
	HONEYPOTLOGFILE        = "HONEYPOT_LOGFILE"
	DEFAULTHONEYPOTLOGFILE = "/var/log/honeyshim/honeyshim.log"
	SENSORNAME             = "SENSOR_NAME"
	DEFAULTSENSORNAME      = "honeyshim"
)
