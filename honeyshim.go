package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/spf13/viper"
)

var (
	targetApp  string
	localPort  string
	sensorName string
	logFile    string
	recordChan chan Record
)

type Record struct {
	RemoteHost  string      `json:"remotehost"`
	Method      string      `json:"method"`
	Url         string      `json:"url"`
	Headers     http.Header `json:"headers"`
	UserAgent   string      `json:"useragent"`
	Referer     string      `json:"referer"`
	EventTime   uint64      `json:"eventtime"`
	PostValues  url.Values  `json:"postvalues"`
	Destination string      `json:"destination"`
	SensorName  string      `json:"sensorname"`
}

type HoneyLogger struct {
	appLogger *log.Logger
	logFile   *os.File
	records   chan Record
}

func NewHoneyLogger(logfile string, records chan Record) (*HoneyLogger, error) {
	honeylogger := new(HoneyLogger)
	logFile, err := os.OpenFile(logfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		return nil, err
	}
	appLogger := log.New(logFile, "", 0)

	honeylogger.logFile = logFile
	honeylogger.records = records
	honeylogger.appLogger = appLogger

	return honeylogger, nil
}

func buildJSON(r Record) (string, error) {
	data, err := json.Marshal(r)
	if err != nil {
		return "", err
	}

	return string(data), nil
}

func processRequest(r *http.Request) Record {
	data := Record{}
	data.SensorName = sensorName
	data.RemoteHost = r.RemoteAddr
	data.Method = r.Method
	data.Url = r.RequestURI
	data.Headers = r.Header
	data.UserAgent = r.UserAgent()
	data.Referer = r.Referer()
	data.EventTime = uint64(time.Now().Unix())
	data.Destination = r.Host
	r.ParseForm()
	data.PostValues = r.PostForm
	return data
}

func (hl *HoneyLogger) ProcessLogs() {
	for record := range hl.records {
		recordJson, _ := buildJSON(record)
		hl.appLogger.Println(recordJson)
	}
}

func handleIndex(w http.ResponseWriter, r *http.Request) {
	fmt.Println("generating json log")
	record := processRequest(r)
	recordChan <- record

	fmt.Printf("Processing: %s %s\n", r.URL.String(), r.RemoteAddr)
	targetURL := targetApp + r.URL.String()
	fmt.Printf("Getting data: %s\n", targetURL)

	remoteData, err := http.Get(targetURL)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	defer remoteData.Body.Close()

	data, err := ioutil.ReadAll(remoteData.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	fmt.Fprint(w, string(data))
}

func loadConfigs() error {
	viper.SetConfigType("env")
	viper.SetConfigFile(".env")
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		fmt.Println("Error reading .env file for settings.")
		_, newErr := os.Stat(".env")
		if newErr == nil {
			return err
		}

		fmt.Println(".env file does not exist, continuing.")
	}
	viper.SetDefault(TARGETAPP, DEFAULTTARGETAPP)
	targetApp = viper.GetString("TARGET_APP")
	viper.SetDefault("LOCAL_PORT", ":8080")
	localPort = viper.GetString("LOCAL_PORT")
	viper.SetDefault(HONEYPOTLOGFILE, DEFAULTHONEYPOTLOGFILE)
	logFile = viper.GetString(HONEYPOTLOGFILE)
	viper.SetDefault(SENSORNAME, DEFAULTSENSORNAME)
	sensorName = viper.GetString(SENSORNAME)

	return nil
}

func main() {
	fmt.Println("Launching HoneyShim")

	if err := loadConfigs(); err != nil {
		log.Fatal(err)
	}

	recordChan = make(chan Record, 100)
	honeyLogger, err := NewHoneyLogger(logFile, recordChan)
	if err != nil {
		log.Fatal(err)
	}
	go honeyLogger.ProcessLogs()

	http.HandleFunc("/", handleIndex)
	if err := http.ListenAndServe(localPort, nil); err != nil {
		log.Fatal(err)
	}

}
