HoneyShim
---

HoneyShim is a honey pot proxy server that sits in front of a REST based webapp.

All request data is logged to a JSONL file for easy analysis.

## Configuration
HoneyShim is configured using the following environment varables

``` ini
LOCAL_PORT - What port should HoneyShim listen on, default ":8080"

TARGET_APP - Base URL to forward all requests to, default "http://localhost:8000"

HONEYPOT_LOGFILE - File path for honeypot log file, default "/var/log/honeyshim/honeyshim.log"

SENSOR_NAME - The name of the honeypot that recorded this event, assuming there are multiple honeypots deployed, default 'honeyshim'
```

NOTE: HoneyShim does not rotate honeypot log files
